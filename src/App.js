import React,{useState} from 'react';
import './App.css';
import Title from './components/Title';
import Button from './components/Button';
import './Index.css';
import Paragraph from './components/Paragraph';
//Components import

function App(props) {

  const [showTitle, setShowTitle] = useState(false);

  var buttonColors = [
    {color:'#FFFFFF',bgColor:'#F44336'},
    {color:'#FFFFFF',bgColor:'#E91E63'},
    {color:'#FFFFFF',bgColor:'#9C27B0'},
    {color:'#FFFFFF',bgColor:'#673AB7'},
    {color:'#FFFFFF',bgColor:'#3F51B5'},
    {color:'#000000',bgColor:'#2196F3'},
    {color:'#000000',bgColor:'#03A9F4'},
    {color:'#000000',bgColor:'#00BCD4'},
    {color:'#FFFFFF',bgColor:'#009688'},
    {color:'#FFFFFF',bgColor:'#F44336'},
    {color:'#000000',bgColor:'#4CAF50'},
    {color:'#000000',bgColor:'#8BC34A'},
    {color:'#000000',bgColor:'#CDDC39'},
    {color:'#000000',bgColor:'#FFEB3B'},
    {color:'#000000',bgColor:'#FFC107'},
    {color:'#000000',bgColor:'#FF9800'},
    {color:'#000000',bgColor:'#FF5722'},
    {color:'#FFFFFF',bgColor:'#795548'},
    {color:'#000000',bgColor:'#9E9E9E'},


    
  ]
  return (
    <div className="App">
      {[1,2,3,4,5,6].map((item,index) => (<Title key={index} element={"h"+item} titleText="Milos" color="white" background="green"/>))}
      {showTitle == true && <Title element="h1" titleText="This is title" color="black" background="white"/>}
      <Paragraph text="This is Paragagraph" paraSize="15px" paraColor="green" paraBg="yellow"/>
      <Button btnText="Button" color="green" bgColor="red" ishref='false' onClick={() => setShowTitle(true)}/>
      <div className="buttonColors">
      {buttonColors.map((x,y) => <Button key={y} btnText={x.bgColor} color={x.color} bgColor={x.bgColor} ishref='false'/>)}
      </div>
    </div>
    
  );
}

export default App;