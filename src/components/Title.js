import React from 'react'

function Title(props) {

  var divStyle = {
    color: props.color,
    background: props.background
  };
    
    return ( 
    <React.Fragment>
    <props.element style={divStyle}>Hello, {props.titleText}</props.element>
    </React.Fragment>
        
    )
}



export default Title;


