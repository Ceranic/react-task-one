import React from 'react'
import './Button.css'

function Button(props) {
    let colorText = {
        color: props.color,
        backgroundColor: props.bgColor
    }
    let NewElement = props.ishref === 'true' ? "a": "button";
    return (
        <NewElement onClick={props.onClick} style={colorText}>{props.btnText}</NewElement>
    )
}


export default Button;


