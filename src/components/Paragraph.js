import React from 'react'

function Paragraph(props) {
    let paraSize = {
        fontSize: props.paraSize,
        color: props.paraColor,
        backgroundColor: props.paraBg
    }
    return (
        <p style={paraSize}>{props.text}</p>
    )
}




export default Paragraph;  